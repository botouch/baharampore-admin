import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { EmployeeListComponent } from '../employees/employee-list/employee-list.component';
import { EmployeeService } from '../shared/employee.service';
 import { EmployeesComponent } from '../employees/employees.component';
import { EmployeeComponent } from '../employees/employee/employee.component';


@NgModule({
  declarations: [AdminComponent, EmployeeListComponent, EmployeesComponent, EmployeeComponent],
  imports: [
    SharedModule,
    CommonModule, FormsModule,
    ReactiveFormsModule,
    AdminRoutingModule
  ], providers: [EmployeeService]
})
export class AdminModule { }
