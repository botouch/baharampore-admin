import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { DataService } from '../data.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-orderdetails',
  templateUrl: './orderdetails.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderdetailsComponent implements OnInit {
  selectedFile: File;
  filedata: FormData;
  cat: any = {};
  crud: string;
  orderList: any = [];
  orderid: string;
  cart: any;
  driverList: any;
  userList: any = [];
  shopList: any = [];
  shippingAddress: any;
  constructor(public _services: DataService,
    public toastr: ToastrService,
    private activeRoute: ActivatedRoute,
    private _http: HttpClient, vcr: ViewContainerRef) {
  }
  ngOnInit() {
    this.orderid = this.activeRoute.snapshot.params.id;
    this._services.getOrderById(this.orderid).subscribe((Response: any) => {
      this.cart = Response.response.data[0].cart;
      this.orderList = Response.response.data[0].order;
      this.userList = this.orderList.userid;
      this.shopList = this.orderList.shopid;
      this.getShipping(this.userList.id);
    });
    this.getDriver();
  }
  getDriver() {
    this._services.getDriverList().subscribe((Response: any) => {
      this.driverList = Response.response.data;
    });
  }
  assignDriver(event) {
    const data = {
      id: this.orderid,
      driverid: event.target.value
    };
    this._services.assignDriver(data).subscribe((Response: any) => {
      this.toastr.success(Response.response.message, 'success!');
      this.ngOnInit();
    });
  }
  updateOrder(order, type) {
    const obj = {
      id: order.id,
      type: type,
      value: (type === 'cancel') ? 2 : order[type]
    };
    this.updateOrderStatus(obj);
  }
  updateOrderAll(str) {
    if (str === 'cancel') {
      this.updateSeries(false);
      this.updateOrder(this.orderList, 'cancel');
    } else {
      this.updateSeries(true);
    }
  }
  updateSeries(boolean) {
    const confirmAlert = confirm('Are you sure want to this item ?');
    if (confirmAlert) {
      this.orderList.shop_accept = boolean;
      this.orderList.shop_prepared = boolean;
      this.orderList.driver_accept = boolean;
      this.orderList.driver_picked = boolean;
      this.orderList.user_received = boolean;
      this.updateOrder(this.orderList, 'shop_accept');
      this.updateOrder(this.orderList, 'shop_prepared');
      this.updateOrder(this.orderList, 'driver_accept');
      this.updateOrder(this.orderList, 'driver_picked');
      this.updateOrder(this.orderList, 'user_received');
    }
  }
  updateOrderStatus(obj) {
    this._services.task(obj).subscribe((response: any) => {
      const result = response.response.data;
      this.ngOnInit();
    });
  }
  getShipping(id) {
    const obj = {
      id: id
    };
    this._services.getShippingAddress(obj).subscribe((response: any) => {
      const result = response.response.data;
      this.shippingAddress = result[0];
    });
  }
}
