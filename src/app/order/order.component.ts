import { Location } from '@angular/common';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '../data.service';
@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  rangeDates = [];
  view: boolean;
  orderList: any = [];
  cols: { field: string; header: string; }[];
  result: any;
  totalprice = 0;
  title: any;
  page = 0;
  en: any;
  totalRecords: any;
  constructor(public _services: DataService,
    public toastr: ToastrService, private route: ActivatedRoute, vcr: ViewContainerRef, private location: Location) {
    this.cols = [
      { field: 'orderid', header: 'Order id' },
      { field: 'createdAt', header: 'Order date' },
      { field: 'status', header: 'Order status' },
      { field: 'driverUsername', header: 'Driver' },
      { field: 'username', header: 'User' },
      { field: 'shopname', header: 'Shop' },
      { field: 'price', header: 'Total Price' }
    ];
    this.route.queryParams.subscribe(params => {
      if (params.type) {
        this.title = params.type.replace('-', ' ');
      }
    });
  }
  back() {
    this.ngOnInit();
    this.location.back();
  }
  ngOnInit() {
    this.en = {
      firstDayOfWeek: 0,
      dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
      dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
      dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
      monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'September', 'October', 'November', 'December'],
      monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      today: 'Today',
      clear: 'Clear',
      dateFormat: 'mm/dd/yy',
      weekHeader: 'Wk'
    };
    const date = new Date();
    // const startDate = new Date(date.getFullYear(), date.getMonth(), 1);
    const startDate = date;
    const endDate = date;
    if (this.rangeDates.length === 0) {
      this.rangeDates = [startDate, endDate];
    }
    this.getOrder();
  }
  formatDate(date) {

    return moment(date).format('YYYY-MM-DD');
  }
  getOrder() {
    const obj = {
      page: this.page,
      start: this.formatDate(this.rangeDates[0]),
      end: this.formatDate(this.rangeDates[1])
    };
    this._services.getOrder(obj).toPromise().then((Response: any) => {
      this.result = Response.response.data;
      this.totalRecords = Response.response.message.totalRecords;
      this.setData();
    });
  }
  setData() {
    this.totalprice = 0;
    this.orderList = [];
    const order = this.result.map((item) => item.order);
    Object.keys(this.result).forEach((key) => {
      order[key].shopname = order[key].shopid.title;
      order[key].username = `${order[key].userid.firstname} ${order[key].userid.lastname}`;
      order[key].price = this.price(order[key].totalprice);
      this.totalprice += order[key].totalprice;
      order[key].status = this.orderShipping(order[key].status)
      order[key].createdAt = moment(order[key].createdAt).format('lll');
      order[key].driverUsername = (order[key].driverid) ? `${order[key].driverid.driver_first_name}
        ${order[key].driverid.driver_last_name}` : '';
    });
    if (this.rangeDates) {
      this.totalprice = 0;
      const orderList = order.filter((item) => {

        const startDate = moment(item.createdAt).isSameOrAfter(this.rangeDates[0], 'day');
        const endDate = moment(item.createdAt).isSameOrBefore(this.rangeDates[1], 'day');
        if (startDate && endDate) {
          this.totalprice += item.totalprice;
          return true;
        }
      });
      this.createList(orderList);
    } else {
      this.createList(order);
    }
  }
  orderShipping(str) {
    if (str === 0) {
      return 'Pending';
    }
    if (str === 1) {
      return 'Delivered';
    }
    if (str === 2) {
      return 'Cancelled';
    }
  }
  createList(order) {
    order.sort((a, b): any => {
      const date1 = new Date(a.createdAt);
      const date2 = new Date(b.createdAt);
      return date2.getTime() - date1.getTime();
    });
    this.orderList = order;
  }
  price(amount) {
    const formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'INR',
      minimumFractionDigits: 2,
    });
    return formatter.format(amount);
  }
  paginate(event) {
    this.page = event.first;
    this.ngOnInit();
  }
}
