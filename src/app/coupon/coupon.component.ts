import { Component,  OnInit, ViewContainerRef } from '@angular/core';
import { DataService } from '../data.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { ExportToCsv } from 'export-to-csv';
import * as moment from 'moment';

@Component({
  selector: 'app-coupon',
  templateUrl: './coupon.component.html',
  styleUrls: ['./coupon.component.css']
})
export class CouponComponent implements OnInit {
  title = 'app';
  userId: string;
  username: string;
  crud: string;
  coupon: any = {};
  couponsList: any = [];
  filteredCoupons: any = [];
  private _searchTerm: any;
  category: any = [];
  constructor(
    public _services: DataService, public toastr: ToastrService, vcr: ViewContainerRef) {
  }
  ngOnInit() {
    this._services.getCoupon().toPromise().then((Response: any) => {
      const result = Response.response.data;

      this.couponsList = result;
      this.filteredCoupons = result;
    });
  }
  addCoupon() {

    if (this.crud === 'edit') {
      this._services.editCoupon(this.coupon, this.crud).toPromise().then(this.commonResponse());
    } else {
      this._services.addCoupon(this.coupon).toPromise().then(this.commonResponse());
    }

    }
    private commonResponse(): (value: Object) => void {
      return (Response: any) => {
        if (Response.success === false) {
          this.toastr.error(Response.message.message, 'Alert!');
        } else {
          this.coupon = {};
          this.toastr.success(Response.message, 'Success!');
          this.ngOnInit();
        }
      };
    }
  edit(item) {
    this.crud = 'edit';
    this.coupon = item;
  }
  action(type) {
    this.coupon = {};
    this.crud = type;
  }
  delete(item) {
    const confirmAlert = confirm('Are you sure want to delete this coupon ?');
    if (confirmAlert === true) {
      const tbl = 'coupon';
      const obj = {
        id: item.id
      };
      this._services.deleteSails(tbl, obj).toPromise().then((response: any) => {
        if (response.success === false) {
          this.toastr.error(response.message, 'Alert!');
        } else {
          this.toastr.success(response.message, 'Success!');
          this.ngOnInit();
        }
      });
    }
  }
}
