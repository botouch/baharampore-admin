import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CouponRoutingModule } from './coupon-routing.module';
import { CouponComponent } from './coupon.component';
import { SharedModule } from '../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [CouponComponent],
  imports: [ SharedModule,
    CommonModule,
    CouponRoutingModule, FormsModule, ReactiveFormsModule
  ]
})
export class CouponModule { }
