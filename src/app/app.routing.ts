import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AuthGuardService } from './auth-guard.service';
const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'admin',
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'users',
    loadChildren: () => import('./users/users.module').then(m => m.UsersModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'driver',
    loadChildren: () => import('./driver/driver.module').then(m => m.DriverModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'category',
    loadChildren: () => import('./category/category.module').then(m => m.CategoryModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'order',
    loadChildren: () => import('./order/order.module').then(m => m.OrderModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'promote',
    loadChildren: () => import('./promote/promote.module').then(m => m.PromoteModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'product-category/:id',
    loadChildren: () => import('./productcategory/productcategory.module').then(m => m.ProductcategoryModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'product/:id',
    loadChildren: () => import('./product/product.module').then(m => m.ProductModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'post',
    loadChildren: () => import('./post/post.module').then(m => m.PostModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'add-post',
    loadChildren: () => import('./add-post/add-post.module').then(m => m.AddPostModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'image',
    loadChildren: () => import('./image/image.module').then(m => m.ImageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'placeapi', loadChildren: () => import('./placedata/placedata.module').then(m => m.PlacedataModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'banner',
    loadChildren: () => import('./banner/banner.module').then(m => m.BannerModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'product-tag',
    loadChildren: () => import('./product-tag/product-tag.module').then(m => m.ProductTagModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'coupon',
    loadChildren: () => import('./coupon/coupon.module').then(m => m.CouponModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'meat/:id',
    loadChildren: () => import('./meat-category/meat-category.module').then(m => m.MeatCategoryModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'grocery/:id',
    loadChildren: () => import('./grocery-essential/grocery-essential.module').then(m => m.GroceryEssentialModule),
    canActivate: [AuthGuardService]
  },
  // {    path: 'chat', loadChildren: './chat/chat.module#ChatModule' ,    },
  {
    path: '**',
    component: PageNotFoundComponent
  },
];
@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { enableTracing: false })],
  exports: [RouterModule]
})
export class AppRouting { }
