import { Pipe, PipeTransform } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { log } from 'util';
@Pipe({
  name: 'filterorder'
})
export class FilterorderPipe implements PipeTransform {
  constructor(public route: ActivatedRoute) { }
  transform(value: any, args?: any): any {
    let order = [];
    this.route.queryParams.subscribe(params => {
      if (params.type === 'completed-order') {
        order = value.filter(item => item.user_received === 1);
      }
      if (params.type === 'pending-order') {
        order = value.filter(item => item.user_received === 0);
      }
      if (params.type === 'total-order') {
        order = value;
      }
    });
    return order;
  }
}
