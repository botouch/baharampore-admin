import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { DataService } from '../data.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
})
export class ProductComponent implements OnInit {
  selectedFile: any;
  filedata: FormData;
  cat: any = {};
  crud: string;
  Product: any = [];
  categoryid: any;
  tags: any = [];
  constructor(
    public _services: DataService, public toastr: ToastrService, private route: ActivatedRoute, vcr: ViewContainerRef) {
    this.categoryid = this.route.snapshot.params.id;
  }
  ngOnInit() {
    const obj = {
      id: this.categoryid
    };
    this._services.getProduct(obj).toPromise().then((Response: any) => {
      this.Product = Response.response.data;
    });
    this._services.getProductTag().toPromise().then((Response: any) => {
      this.tags = Response.response.data;
      this.tags.unshift({
        'tag': 'Choose',
      });
    });
  }
  change_active_status(val, item) {
    const active_obj = {
      _id: item.id,
      active: val.checked
    };
    this._services.updateProductActiveStaus(active_obj).toPromise().then((Response: any) => {
      if (Response.success === false) {
        if (Response.message.message) {
          this.toastr.error(Response.message.message, 'Alert!');
        }
        this.toastr.error(Response.message, 'Alert!');
      } else {
        this.toastr.success(Response.message, 'Success!');
      }
    });
  }
  addCategory() {
    this.cat.category = this.categoryid;
    if (this.crud === 'edit') {
      this._services.addProduct(this.cat, this.crud).toPromise().then((Response: any) => {
        this.toastr.success(Response.response.message, 'Success!');
        this.ngOnInit();
      });
    } else {
      this._services.addProduct(this.cat, this.crud).toPromise().then((Response: any) => {
        this.toastr.success(Response.response.message, 'Success!');
        this.ngOnInit();
      });
    }
  }
  fileChange(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      const formData: FormData = new FormData();
      formData.append('sampleFile', file, file.name);
      this.filedata = formData;
    }
  }
  edit(item) {
    this.crud = 'edit';
    this.cat = item;
    this._services.getTags(this.cat._id).toPromise().then((Response) => {
    });
  }
  action(type) {
    this.cat = {};
    this.crud = type;
  }
  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }
  delete(item) {
    const confirmAlert = confirm('Are you sure want to this item ?');
    if (confirmAlert === true) {
      const tbl = 'product';
      const obj = {
        id: item.id
      };
      this._services.deleteSails(tbl, obj).toPromise().then((response: any) => {
        if (response.success === false) {
          this.toastr.error(response.message, 'Alert!');
        } else {
          this.toastr.success(response.message, 'Success!');
          this.ngOnInit();
        }
      });
    }
  }
  addImage(event, item) {
    const uploadData = new FormData();
    this.selectedFile = event.target.files;
    for (let i = 0; i < this.selectedFile.length; i++) {
      uploadData.append('product_image', this.selectedFile[i], this.selectedFile[i]['name']);
    }
    this._services.addProductImage(uploadData, item).toPromise().then((Response: any) => {
      if (Response.success === false) {
        this.toastr.error(Response.message.message, 'Alert!');
      } else {
        this.toastr.success(Response.message, 'Success!');
        this.ngOnInit();
      }
    }, (Error) => {
    });
  }
  tagUpdated(event, item) {
    const obj = { tag: event.value };
    this._services.updateTag(obj, item).toPromise().then((response) => {
    });
  }
}
