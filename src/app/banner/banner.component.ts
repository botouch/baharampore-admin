import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { DataService } from '../data.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {
  filteredOptionsData: any = [];
  selectedFile: File[];
  filedata: FormData;
  banner: any = [];
  bannerList = [
    { title: 'Banner 1', value: 1 },
    { title: 'Banner 2', value: 2 },
    { title: 'Banner 3', value: 3 },
  ];
  selectedItem: any = {};
  bannerFilter: any = [];
  bannertype: number;
  constructor(public _services: DataService, public toastr: ToastrService, private _http: HttpClient, vcr: ViewContainerRef) {
  }
  ngOnInit() {
    this.getBanner();
    this.getPost();
  }
  getBanner() {
    this._services.getBanner().toPromise().then((response: any) => {
      this.banner = response.response.data;
      this.bannerChange(this.bannerList[0]);
    });
  }
  getPost() {
    this._services.getPost({ id: undefined }).toPromise().then((response: any) => {
      const result = response.data;
      this.filteredOptionsData = result.map((item) => {
        const obj = Object.assign({});
        obj.shopName = `${item.title} ${item.phone}`;
        obj._id = item._id;
        obj.phone = item.phone;
        return obj;
      });
    });
  }
  addImage(event) {
    const uploadData = new FormData();
    this.selectedFile = event.target.files;
    for (const element of this.selectedFile) {
      uploadData.append('banner_image', element, element['name']);
    }
    this._services.addBannerimage(uploadData, this.bannertype).toPromise().then((response: any) => {
      if (response.success === false) {
        this.toastr.error(response.message.message, 'Alert!');
      } else {
        this.toastr.success(response.message, 'Success!');
      }
    }, (Error) => {
    });
  }
  delete(item) {
    const confirmAlert = confirm('Are you sure want to delete this item ?.');
    if (confirmAlert) {
      this._services.deleteBanner({ id: item.id }).toPromise().then((response: any) => {
        if (response.success === false) {
          this.toastr.error(response.message, 'Alert!');
        } else {
          this.toastr.success(response.message, 'Success!');
          this.ngOnInit();
        }
      });
    }
  }
  updateBanner(data) {
    const obj = {
      linkto: data._id,
      id: this.selectedItem.id
    };
    this._services.linkTo(obj).toPromise().then((response: any) => {
      this.toastr.success('updated ! Banner linked to shop', 'Success!');
      this.ngOnInit();
    });
  }
  bannerChange(event) {
    this.bannerFilter = [];
    const list = this.banner;
    const bannerFilter = list.filter((item) => item.bannerindex === event.value)
      .map((item) => {
        const obj = Object.assign({});
        obj.title = (item.linkto !== null) ? item.linkto.title : '';
        obj.bannerindex = item.bannerindex;
        obj.id = item.id;
        obj.image = item.image;
        return obj;
      });
    this.bannerFilter = bannerFilter;
    this.bannertype = event.value;
  }
}
