import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { DataService } from '../data.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { SecurityManagerService } from '../security-manager.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  pageName = {
    'Total Order': 'order?type=total-order',
    'TOTAL REVENUE': 'order',
    'Completed Order': 'order?type=completed-order',
    'Pending Order': 'order?type=pending-order',
    'Reviews': '',
    'Post': 'post',
    'Product Categories': 'product-category/undefined',
    'Product': 'product-category/undefined',
    'Users': 'users',
    'Categories': 'category',
    'Driver': 'driver',
    'Rejected Order': 'order?type=rejected-order',
    'Push notification': ''
  };
  shippingStatus = [
    { label: 'Open', value: 'open' },
    { label: 'Closed', value: 'closed' },
  ];
  dashbordCount: any = [];
  rangeDates = [];
  charges = {};
  add: boolean;
  view: boolean;
  orderList: any = [];
  cols: { field: string; header: string; }[];
  result: any;
  totalprice = 0;
  title: any;
  page = 0;
  en: any;
  private _edit: boolean;
  totalRecords: any;
  settingData: any = [];
  settingCols: { field: string; header: string; }[];
  access: any = [
    // {
    //   "route": "Order Management",
    //   "_id": "60e9ad7ea0a36e2d6be577bd",
    //   "privilege": "true",
    //   "admin": "60e74a573e907b35517f4fcb",
    //   "createdAt": "2021-07-10T14:23:58.911Z",
    //   "updatedAt": "2021-07-10T14:23:58.911Z",
    //   "__v": 0
    // },
    // {
    //   "route": "Dashboard",
    //   "_id": "60e9ada2a0a36e2d6be577be",
    //   "privilege": "false",
    //   "admin": "60e74a573e907b35517f4fcb",
    //   "createdAt": "2021-07-10T14:24:34.499Z",
    //   "updatedAt": "2021-07-10T14:24:34.499Z",
    //   "__v": 0
    // }
  ];
  constructor(public route: Router,
    public _services: DataService, public toastr: ToastrService, vcr: ViewContainerRef, public security: SecurityManagerService) {
    this.cols = [
      { field: 'orderid', header: 'Order id' },
      { field: 'createdAt', header: 'Order date' },
      { field: 'status', header: 'Order status' },
      { field: 'driverUsername', header: 'Driver' },
      { field: 'username', header: 'User' },
      { field: 'shopname', header: 'Shop' },
      { field: 'price', header: 'Total Price' }
    ];
    this.settingCols = [
      { field: 'key', header: 'Regions' },
      { field: 'value', header: 'Rate' },
      { field: 'status', header: 'Shipping' },
      { field: 'msg', header: 'Comment' }
    ];
  }
  ngOnInit() {
    this._services.getDashbordRecord().toPromise().then((response: any) => {
      const record = response.response.data;
      this.dashbordCount = this.dyanmicColorSetValue(record);
    });
    const startDate = new Date();
    const endDate = new Date();
    if (this.rangeDates.length === 0) {
      this.rangeDates = [startDate, endDate];
    }
    this.getOrder();
    this.getSetting();
  }
  dyanmicColorSetValue(record: any) {
    const r = [];
    const color = ['blue', 'yellow', 'purple', 'maroon', 'green', 'orange', 'fuchsia', 'olive'];
    Object.keys(record).forEach((item) => {
      const number = Math.floor(Math.random() * 7) + 1;
      r.push({ color: `bg-${color[number]}`, name: [item], count: record[item] });
    });
    return r;
  }
  showPage(item) {
    for (const property in this.pageName) {
      if (property === item.name[0] && this.pageName[property]) {
        this.route.navigateByUrl(this.pageName[property]);
      }
    }
  }
  formatDate(date) {
    return moment(date).format('YYYY-MM-DD');
  }
  getOrder() {
    const obj = {
      page: this.page,
      start: this.formatDate(this.rangeDates[0]),
      end: this.formatDate(this.rangeDates[1])
    };
    this._services.getOrder(obj).toPromise().then((Response: any) => {
      this.result = Response.response.data;
      this.totalRecords = Response.response.message.totalRecords;
      this.setData();
    });
  }
  setData() {
    this.totalprice = 0;
    this.orderList = [];
    const order = this.result.map((item) => item.order);
    Object.keys(this.result).forEach((key) => {
      order[key].shopname = order[key].shopid.title;
      order[key].username = `${order[key].userid.firstname} ${order[key].userid.lastname}`;
      order[key].price = this.price(order[key].totalprice);
      this.totalprice += order[key].totalprice;
      order[key].status = (order[key].status === 1) ? 'delivered' : 'pending';
      order[key].createdAt = moment(order[key].createdAt).format('lll');
      order[key].driverUsername = (order[key].driverid) ? `${order[key].driverid.driver_first_name}
        ${order[key].driverid.driver_last_name}` : '';
    });
    this.orderList = this.createList(order);
  }
  price(amount) {
    const formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'INR',
      minimumFractionDigits: 2,
    });
    return formatter.format(amount);
  }
  getSetting() {
    this._services.getSetting().toPromise().then((response: any) => {
      this.settingData = this.createList(response.response.data);
    });
  }
  createList(item) {
    item.sort((a, b): any => {
      const date1 = new Date(a.createdAt);
      const date2 = new Date(b.createdAt);
      return date2.getTime() - date1.getTime();
    });
    return item;
  }
  addShippingCharges() {
    this._services.addShippingChargesValue(this.charges).toPromise().then((Response: any) => {
      this.getSetting();
      this.charges = {};
    });
  }
  delete(item) {
    const confirmAlert = confirm('Are you sure want to this item ?');
    if (confirmAlert) {
      const tbl = 'setting';
      const obj = {
        id: item.id
      };
      this._services.deleteShippingCharges(tbl, obj).toPromise().then((response: any) => {
        if (response.success === false) {
          this.toastr.error(response.message, 'Alert!');
        } else {
          this.toastr.success(response.message, 'Success!');
          this.getSetting();
        }
      });
    }
  }
  edit(item) {
    this._edit = true;
    this.add = !this.add;
    this.charges = item;
  }
  editShippingCharges() {
    const tbl = 'setting';
    if (this.charges['status'] === 'closed' && !this.charges['msg']) {
      this.toastr.warning('Please add comment for closed', 'Alert!');
      return;
    }
    this._services.editShippingChargesValue(tbl, this.charges).toPromise().then((Response: any) => {
      this.getSetting();
      this.charges = {};
      this.add = !this.add;
    });
  }
  resetShippingForm() {
    this.charges = {};
    this._edit = false;

  }
  checkPrivilege(route){
    return this.security.canExecute(route);
  }
}
