import { Injectable, Output, EventEmitter } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt/src/jwthelper.service';
@Injectable()
export class AuthGuardService implements CanActivate {
  @Output() notifyParent: EventEmitter<any> = new EventEmitter();

  constructor(public router: Router) { }
  canActivate() {
    return this.checkLogin();
  }
  checkLogin(): boolean {
    const helper = new JwtHelperService();
    const myRawToken = sessionStorage.getItem('token');
    if (myRawToken != null) {
      const isExpired = helper.isTokenExpired(myRawToken);
      return !isExpired;
    } else {
      this.router.navigateByUrl('/');
      return false;
    }
  }
  sendNotification(value) {

    this.notifyParent.emit(value);
  }
}
