export class Employee {
    _id: number;
    FullName: string;
    email: string;
    password: string;
    EMPCode: string;
    Mobile: string;
    Position: string;
    Role: string;
    region: string;
}
