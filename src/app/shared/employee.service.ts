import { Injectable } from '@angular/core';
import { Employee } from './employee.model';
import { HttpClient } from '@angular/common/http';
import { config } from '../config';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  formData: Employee;
  list: Employee[];

  constructor(public http: HttpClient) { }

  postEmployee(formData: Employee) {
   return this.http.post(config.API_ENDPOINT + '/register', formData);

  }

  refreshList() {
    return  this.http.get(config.API_ENDPOINT + '/Employee');
  }

  putEmployee(formData: Employee) {
    return this.http.put(config.API_ENDPOINT + '/Employee/' + formData._id, formData);

   }

   deleteEmployee(obj) {
    return this.http.post(config.API_ENDPOINT + '/deleteEmployee/', obj);
   }
}
