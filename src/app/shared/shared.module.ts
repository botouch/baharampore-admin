import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidemenuComponent } from '../sidemenu/sidemenu.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { TooltipModule } from 'primeng/tooltip';
import { DropdownModule } from 'primeng/dropdown';
import { KeyFilterModule } from 'primeng/keyfilter';
import { NgxEditorModule } from 'ngx-editor';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { RatingModule } from 'primeng/rating';
import { ToolbarModule } from 'primeng/toolbar';
import { CalendarModule } from 'primeng/calendar';
import { CardModule } from 'primeng/card';

import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { FileUploadModule } from 'primeng/fileupload';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FieldsetModule } from 'primeng/fieldset';
import { PaginatorModule } from 'primeng/paginator';
import { MultiSelectModule } from 'primeng/multiselect';
import { DialogModule } from 'primeng/dialog';
import { SelectButtonModule } from 'primeng/selectbutton';


@NgModule({
  imports: [TableModule, ButtonModule, CalendarModule, RatingModule, ToolbarModule, TooltipModule, DropdownModule, KeyFilterModule,
    CommonModule, RouterModule, FieldsetModule, ToggleButtonModule, MessagesModule,
    PaginatorModule, MessageModule, InputSwitchModule, FileUploadModule, CardModule, MultiSelectModule, DialogModule, SelectButtonModule],
  declarations: [SidemenuComponent],
  exports: [TableModule, SidemenuComponent, CalendarModule, FieldsetModule, PaginatorModule,
    ToolbarModule, MessagesModule, MessageModule, RatingModule, KeyFilterModule, ToggleButtonModule, InputSwitchModule, CardModule,
    DropdownModule, TooltipModule, NgxEditorModule, ButtonModule, FormsModule, MaterialModule, FileUploadModule, MultiSelectModule, 
    SelectButtonModule, DialogModule]
})
export class SharedModule { }
