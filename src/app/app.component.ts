import { Component } from '@angular/core';
import { AuthGuardService } from './auth-guard.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  loading = false;
  constructor( public _auth: AuthGuardService
  ) {
    this._auth.notifyParent.subscribe((result) => {
      this.getNotification(result);
    });

  }
  getNotification(evt) {
    this.loading = evt;
    // Do something with the notification (evt) sent by the child!
}
}
