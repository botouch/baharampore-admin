import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GroceryEssentialComponent } from './grocery-essential.component';

const routes: Routes = [{
  path: '', component: GroceryEssentialComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroceryEssentialRoutingModule { }
