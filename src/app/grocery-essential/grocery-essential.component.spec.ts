import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroceryEssentialComponent } from './grocery-essential.component';

describe('GroceryEssentialComponent', () => {
  let component: GroceryEssentialComponent;
  let fixture: ComponentFixture<GroceryEssentialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroceryEssentialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroceryEssentialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
