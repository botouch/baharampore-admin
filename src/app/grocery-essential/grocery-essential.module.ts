import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GroceryEssentialRoutingModule } from './grocery-essential-routing.module';
import { GroceryEssentialComponent } from './grocery-essential.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [GroceryEssentialComponent],
  imports: [ SharedModule,
    CommonModule,
    GroceryEssentialRoutingModule
  ]
})
export class GroceryEssentialModule { }
