import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MeatCategoryComponent } from './meat-category.component';

const routes: Routes = [{
  path: '', component: MeatCategoryComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MeatCategoryRoutingModule { }
