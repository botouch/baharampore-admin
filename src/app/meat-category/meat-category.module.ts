import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MeatCategoryRoutingModule } from './meat-category-routing.module';
import { MeatCategoryComponent } from './meat-category.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [MeatCategoryComponent],
  imports: [ SharedModule,
    CommonModule,
    MeatCategoryRoutingModule
  ]
})
export class MeatCategoryModule { }
