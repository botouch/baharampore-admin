import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { DataService } from '../data.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-meat-category',
  templateUrl: './meat-category.component.html',
  styleUrls: ['./meat-category.component.css']
})
export class MeatCategoryComponent implements OnInit {

  selectedFile: File;
  filedata: FormData;
  singleSelect: any;
  cat: any = {};
  crud: string;
  ProductCategory: any = [];
  categoryid: any;
  config = {
    displayKey: 'title',
    search: true,
  };
  post: any = [];
  cols: { field: string; header: string; }[];
  title: any;
  constructor(public _services: DataService,
    public toastr: ToastrService,
    private activeRoute: ActivatedRoute,
    vcr: ViewContainerRef, private router: Router) {
    this.categoryid = this.activeRoute.snapshot.params.id;
    if (this.activeRoute.firstChild != null) {
      this.activeRoute.firstChild.params.subscribe(params => {
        if (params !== null) {
          this.categoryid = params.id;
        }
      });
    }
  }

  ngOnInit() {
    this.cols = [
      { field: 'title', header: 'title' },
      { field: 'shop', header: 'shop name' },
    ];
    const data = {
      id: undefined
    };
    this._services.getPost(data).toPromise().then((Response: any) => {
      this.post = this.filterResturant(Response.data);
      this.singleSelect = this.post[0];
      this.title = this.post[0].title;
      if (this.categoryid === 'undefined') {
        this.changeValue(this.post[0]._id);
      } else {
        this.changeValue(this.categoryid);
      }
    });
  }
  filterResturant(response) {
    return response.filter((item) => {
      if (item.category) {
        return item.category.title === 'Meat';
      }
    });
  }
  addCategory() {
    if (this.categoryid === 'undefined') {
      this.toastr.error('Please select Restaurant', 'Alert!');
      return false;
    }
    this.cat.shopid = this.categoryid;
    if (this.crud === 'edit') {
      this._services.updateProductCategory(this.cat)
        .toPromise().then((response) => this.commonResponse(response));
    } else {
      this._services.addproductcategory(this.cat, this.crud)
        .toPromise().then((response) => this.commonResponse(response));
    }
  }
  private commonResponse(response) {
    this.cat = {};
    this.ngOnInit();
  }
  fileChange(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      const formData: FormData = new FormData();
      formData.append('sampleFile', file, file.name);
      this.filedata = formData;
    }
  }
  edit(item) {
    this.crud = 'edit';
    this.cat = item;
    this._services.getTags(this.cat._id).toPromise().then((Response) => {
    });
  }
  action(type) {
    this.cat = {};
    this.crud = type;
  }
  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }
  delete(item) {
    const confirmAlert = confirm('Are you sure want to delete this item ?');
    if (confirmAlert === true) {
      const tbl = 'product-category';
      const obj = {
        id: item.id
      };
      this._services.deleteSails(tbl, obj).toPromise().then((response: any) => {
        if (response.success === false) {
          this.toastr.error(response.message, 'Alert!');
        } else {
          this.toastr.success(response.message, 'Success!');
          this.ngOnInit();
        }
      });
    }
  }
  changeValue(id) {
    this.categoryid = id;
    this.ProductCategory = [];
    const obj = {
      shopid: id
    };
    this.title = this.singleSelect.title;
    this._services.getProductCategory(obj).toPromise().then((response: any) => {
      this.ProductCategory = response.response.data;
    });
  }

}
