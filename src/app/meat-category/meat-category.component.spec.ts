import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeatCategoryComponent } from './meat-category.component';

describe('MeatCategoryComponent', () => {
  let component: MeatCategoryComponent;
  let fixture: ComponentFixture<MeatCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeatCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeatCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
