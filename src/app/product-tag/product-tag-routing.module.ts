import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductTagComponent } from './product-tag.component';

const routes: Routes = [{
  path: '',
  component: ProductTagComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductTagRoutingModule { }
