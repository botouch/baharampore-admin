import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductTagRoutingModule } from './product-tag-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ProductTagComponent } from './product-tag.component';

@NgModule({
  declarations: [ProductTagComponent],
  imports: [
    CommonModule, SharedModule,
    ProductTagRoutingModule
  ]
})
export class ProductTagModule { }
