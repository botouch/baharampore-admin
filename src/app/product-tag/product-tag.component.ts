import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { DataService } from '../data.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-product-tag',
  templateUrl: './product-tag.component.html',
  styleUrls: ['./product-tag.component.css']
})
export class ProductTagComponent implements OnInit {
  selectedFile: File;
  filedata: FormData;
  cat: any = {};
  display = false;
  crud: string;
  category: any = [];
  cols: { field: string; header: string; }[];
  image: any;
  selectedItem: any =
    {};
  constructor(public _services: DataService, public toastr: ToastrService, private _http: HttpClient, vcr: ViewContainerRef) {
  }
  ngOnInit() {
    this.cols = [
      { field: 'title', header: 'title' },
    ];
    this._services.getAllProductImage().toPromise().then((Response: any) => {
      this.image = Response.response.data;
    });
    this._services.getProductTag().toPromise().then((Response: any) => {
      const category = Response.response.data;
      this.category = category.sort((a, b) => (b.tag > a.tag) ? -1 : 1);
    });
  }
  addCategory() {
    if (this.crud === 'edit') {
      this._services.editProductTag(this.cat).toPromise().then(this.commonResponse());
    } else {
      this._services.addProductTag(this.cat).toPromise().then(this.commonResponse());
    }
  }
  private commonResponse(): (value: Object) => void {
    return (Response: any) => {
      this.cat = {};
      this.ngOnInit();
    };
  }
  edit(item) {
    this.crud = 'edit';
    this.cat = item;
    this._services.getTags(this.cat._id).toPromise().then((Response) => {
    });
  }
  action(type) {
    this.cat = {};
    this.crud = type;
  }
  delete(item) {
    const confirmAlert = confirm('Are you sure want to this item ?');
    if (confirmAlert) {
      const obj = {
        id: item.id
      };
      this._services.deleteProductTag(obj).toPromise().then((response: any) => {
        if (response.success === false) {
          this.toastr.error(response.message, 'Alert!');
        } else {
          this.toastr.success(response.message, 'Success!');
          this.ngOnInit();
        }
      });
    }
  }
  change_active_status(val, item) {
    const active_obj = {
      id: item.id,
      active: val.checked,
      tag: item.tag
    };
    this._services.editProductTag(active_obj).toPromise().then(this.commonResponse());
  }
  showDialog(item) {
    this.selectedItem = item;
    this.display = true;
  }
  navigateToResource(list): void {
    this.display = false;
    const item = this.selectedItem;
    const active_obj = {
      id: item.id,
      active: item.active,
      tag: item.tag,
      image: list.image
    };
    this._services.editProductTag(active_obj).toPromise().then(this.commonResponse());
  }
}
