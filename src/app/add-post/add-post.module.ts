import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EditorModule} from 'primeng/editor';

import { AddPostRoutingModule } from './add-post-routing.module';
import { AddPostComponent } from './add-post.component';
import { SharedModule } from '../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
@NgModule({
  declarations: [AddPostComponent],
  imports: [FormsModule, SharedModule, EditorModule,
    CommonModule, ReactiveFormsModule,
    AddPostRoutingModule
  ]
})
export class AddPostModule { }
