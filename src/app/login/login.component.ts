import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { DataService } from '../data.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  user: any = {};
  constructor(public router: Router, public service: DataService, ) {
  }
  doLogin(loginForm: NgForm) {
    this.service.doLogin(loginForm.value).toPromise().then((Response: any) => {
      if (Response.sucess) {

        sessionStorage.setItem('token', Response.data.token);
        sessionStorage.setItem('userdetails', JSON.stringify(Response.data.userDetails));
        this.router.navigateByUrl('dashboard');
      } else {
        sessionStorage.clear();
      }
    });
  }

}
