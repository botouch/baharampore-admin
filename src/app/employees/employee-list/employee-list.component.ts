import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/shared/employee.service';
import { Employee } from 'src/app/shared/employee.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  list: any = [];

  constructor(public service: EmployeeService,
    public toastr: ToastrService) { }

  ngOnInit() {
    this.service.refreshList().subscribe((response: any) => {
      this.list = response.response;
    });
  }

  populateForm(emp: Employee) {
    this.service.formData = Object.assign({}, emp);
  }

  onDelete(emp) {
    if (confirm('Are you sure to delete this record?')) {
      const obj = {id: emp._id};
      this.service.deleteEmployee(obj).subscribe(res => {
        this.service.refreshList();
        this.toastr.warning('Deleted successfully', 'EMP. Register');
        this.ngOnInit();
      });
    }
  }

}
