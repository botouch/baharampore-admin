import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/shared/employee.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '../../data.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  region: any [];
  constructor(public service: EmployeeService, public _services: DataService,
    public toastr: ToastrService) { }

  ngOnInit() {
    this.resetForm();
    this.getRegion();
  }

  resetForm(form?: NgForm) {
    if (form != null) {
      form.resetForm();
    }
    this.service.formData = {
      _id: null,
      FullName: '',
      email: '',
      password: '',
      Position: '',
      EMPCode: '',
      Mobile: '',
      Role: '',
      region: ''
    };
  }


  onSubmit(form: NgForm) {
    if (form.value.EmployeeID == null) {
      this.insertRecord(form);
    } else {
      this.updateRecord(form);
    }
  }

  insertRecord(form: NgForm) {
    this.service.postEmployee(form.value).subscribe(res => {
      this.toastr.success('Inserted successfully', 'EMP. Register');
      this.resetForm(form);
      this.service.refreshList();
    });
  }

  updateRecord(form: NgForm) {
    this.service.putEmployee(form.value).subscribe(res => {
      this.toastr.info('Updated successfully', 'EMP. Register');
      this.resetForm(form);
      this.service.refreshList();
    });

  }
  getRegion() {
    this._services.getRegion().toPromise().then((response: any) => {
      this.region = response.data;
    });
  }

}
