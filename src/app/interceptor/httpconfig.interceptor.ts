import { Injectable } from '@angular/core';
import 'rxjs/add/operator/do';
import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { AuthGuardService } from '../auth-guard.service';
@Injectable()
export class Interceptor implements HttpInterceptor {
    constructor(public toastr: ToastrService, public _auth: AuthGuardService) { }
    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        this._auth.sendNotification(true);
        const headers = req.headers
            //    .set('Content-Type', 'application/json')
            .set('x-access-token', `${sessionStorage.getItem('token')}`)
            .set('role', `admin`);
        const cloneReq = req.clone({ headers });
        return next.handle(cloneReq)
            .pipe(
                map((event: HttpResponse<any>) => {
                    if (event instanceof HttpResponse && req.method !== 'GET') {
                        if (event.status === 200) {
                            this.toastr.success(event.body.message, 'Success!');
                        } else {
                            this.toastr.error(event.body.message, 'Alert!');
                        }
                    }
                    this._auth.sendNotification(false);
                    return event;
                }))
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    let errMsg = '';
                    if (error.error instanceof ErrorEvent) {
                        errMsg = `Error: ${error.error.message}`;
                    } else {
                        errMsg = `Error Code: ${error.status},  Message: ${error.error.response.message}`;
                    }
                    this.toastr.error(errMsg, 'Alert!');
                    this._auth.sendNotification(false);
                    return throwError(errMsg);
                },
                )
            );
    }
}
