import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { config } from '../config';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {
  id: any;
  imagedetails: any[];
  post: any = [];
  category: any;
  msg: string;
  selectedFile: Array<File>;

  constructor(public _services: DataService, private activeRoute: ActivatedRoute,
  ) {
    this.activeRoute.queryParams.subscribe(params => {

      this.id = params.id;
    });
  }

  ngOnInit() {
    const imageval = [];

    this._services.getPostDetails(this.id).toPromise().then((response: any) => {
      const result = response.data;

      this.post = response.data.post;
      this.category = response.data.category;

      result.image.forEach(element => {
        element.image = `${config.API_ENDPOINT}/image/postimage/${element.image}`;
        imageval.push(element);
      });
      this.imagedetails = imageval;

    });
  }
  navigateToResource(url): void {
    window.open(url);
  }
  delete(item) {
    const confirmAlert = confirm('Are you sure want to this item ?');
    if (confirmAlert === true) {
      const obj = {
        id: item._id
      };
      this._services.deletePostImage(obj).toPromise().then((response: any) => {


        this.ngOnInit();

      });
    }
  }
  onFileChanged(event) {
    const uploadData = new FormData();
    this.selectedFile = event.target.files;
    for (let i = 0; i < this.selectedFile.length; i++) {
      uploadData.append('uploads', this.selectedFile[i], this.selectedFile[i]['name']);
    }
    uploadData.append('id', this.post._id);
    this._services.updatePostImage(uploadData).toPromise().then((Response: any) => {


      this.ngOnInit();
    }, (Error) => {
    });
  }
}
