import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImageRoutingModule } from './image-routing.module';
import { ImageComponent } from './image.component';
import { SharedModule } from '../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [ImageComponent],
  imports: [SharedModule, FormsModule, ReactiveFormsModule,
    CommonModule,
    ImageRoutingModule
  ]
})
export class ImageModule { }
