import { Component, Injectable, OnInit } from '@angular/core';
import { DataService } from './data.service';
@Injectable()
export class SecurityManagerService {

  access: any;
  data: any;
  constructor(private dataService: DataService) { }
  getAccess() {
    if (!this.data) {
      this.dataService.getAccess().toPromise().then((response: any) => {
        if (response.data) {
          this.data = response.data;
        }
      });
    }
    return this.data;
  }

  canExecute(route) {
    let allowAccess = false;
    const testAccess = this.getAccess();
    if (testAccess) {
      const test = this.getAccess().filter(item => item.route === route);
      console.log(this.getAccess());
      if (test[0] && test[0].privilege) {
        allowAccess = test[0].privilege
        console.log(typeof (allowAccess));
      }
    }
    return allowAccess;

  }
}


