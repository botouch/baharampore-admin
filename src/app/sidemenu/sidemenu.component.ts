import { Component, OnInit, Input} from '@angular/core';
import { Router } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { DataService } from '../data.service';
import { OnDestroy, Renderer2 } from '@angular/core';
import { SecurityManagerService } from '../security-manager.service';


@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.css'],
  providers: [Location, { provide: LocationStrategy, useClass: PathLocationStrategy }]
})
export class SidemenuComponent implements OnInit {
  date: Date;
  @Input() accessValues: any = [];
  constructor(public router: Router,
    private renderer: Renderer2,
    public _services: DataService, private location: Location, public security:SecurityManagerService) { }
  ngOnInit() {
    this.refreshData();
    setInterval(() => {
      this.refreshData();
    }, 500);
  }
  refreshData() {
    this.date = new Date();
  }
  logout() {
    sessionStorage.clear();
    this.router.navigate(['']);
    window.location.reload();
  }
  goBack() {
    this.location.back();
  }
  flushAllRedisMemory() {
    const confirmAlert = confirm('Are you sure want to this item ?');
    if (confirmAlert === true) {
      this._services.flushAllRedisMemory({ task: 'FLASHALL' }).toPromise().then((Response: any) => {
      });
    }
  }
  hideMenu() {
    this.renderer.removeClass(document.body, 'sidebar-open');
  }
  checkPrivilege(route){
    return this.security.canExecute(route);
  }
}
